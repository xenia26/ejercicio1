<?php

define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPSSW', '');
define('DBNAME', 'ejercicio1');

function PDOConnect()
{
    try
    {
        $pdo = new PDO('mysql:host='.DBHOST.';'.'dbname='.DBNAME, DBUSER, DBPSSW);
    }
    catch (PDOException $e)
    {
        echo $e->getMessage();
    }

    return $pdo;
}