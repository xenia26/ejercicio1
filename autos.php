<?php

include_once('config/db.php');
$pdo = PDOConnect();

if (isset($_GET["id"]) && !empty(trim($_GET["id"])) )
{
    $id = $_GET["id"];
    $delete_id = $pdo->prepare("DELETE FROM productos WHERE id = :id");
    $delete_id->bindParam(':id', $id);
    $delete_id->execute();
    header("Location: autos.php");
} 
else
{    
    $query = $pdo->prepare("SELECT *  FROM auto" );
    $query->execute();
    $productos = $query->fetchAll(PDO::FETCH_OBJ);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Auto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">    
</head>
<body>
  
    <div class="container p-5">
       
        <div class="row">            
            <div class="col-md-12">            
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Combustible</th>
                    <th>C. Puertas</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($productos)
                  {
                    foreach ($productos as $prod)
                    {
                      ?>
                      <tr>
                        <td><?=$prod->id?></td>
                        <td><?=$prod->nombre?></td>
                        <td><?=$prod->tipoCombustible?></td>
                        <td><?=$prod->cantidadPuertas?></td>                        
                        <td>
                            <a href="ver_auto.php?id=<?=$prod->id?>" class="text-success">Ver</a>
                            <span>|</span>
                            <a href="editar_auto.php?id=<?=$prod->id?>" class="text-success">Editar</a>
                            <span>|</span>
                            <a href="?id=<?=$prod->id?>" onClick="return confirm('Desea borrar este registro?')" class="text-danger">Eliminar</a>
                        </td>
                      </tr>
                      <?php
                    }
                  }
                  else
                  {
                    ?>
                    <tr>
                      <td colspan="4">No hay registros de autos!</td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</body>
</html>