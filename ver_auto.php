<?php
include_once('config/db.php');
$pdo = PDOConnect();
$_nombre = '';
$_descripcion = '';
$_tipoCombustible = '';
$_cantidadPuertas = '';
$_precio = '';

if(isset($_GET["id"]) && !empty(trim($_GET["id"])))
{
    $id = $_GET["id"];
    $select_id = $pdo->prepare("SELECT * FROM auto WHERE id = :id");
    $select_id->bindParam(':id', $id);
    $select_id->execute();
    $auto = $select_id->fetchAll(PDO::FETCH_OBJ);

    $_nombre = $auto[0]->nombre;
    $_descripcion = $auto[0]->descripcion;
    $_tipoCombustible = $auto[0]->tipoCombustible;
    $_cantidadPuertas = $auto[0]->cantidadPuertas;
    $_precio = $auto[0]->precio;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Producto</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css">    
  <link rel="stylesheet" href="../css/style.css">    
</head>
<body>        
<div class="container p-5">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-2">Nombre</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" value="<?=$_nombre?>" disabled>                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Descripcion</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" value="<?=$_descripcion?>" disabled>                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Tipo Combustible</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="tipo" value="<?=$_tipoCombustible?>" disabled>                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Cantidad Puertas</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="medida" value="<?=$_cantidadPuertas?>" disabled>                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Precio</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="medida" value="<?=$_precio?>" disabled>                
                </div>
            </div>            
        </div>
    </div>
</body>
</html>