<?php
include_once('config/db.php');
$pdo = PDOConnect();
$_id = '';
$_nombre = '';
$_descripcion = '';
$_tipoCombustible = '';
$_cantidadPuertas = '';
$_precio = '';

if($_SERVER["REQUEST_METHOD"] === "POST" && !empty($_POST["id"]))
{
    $id = trim($_POST["id"]);
    $nombre = trim($_POST["nombre"]);
    $descripcion = trim($_POST["descripcion"]);
    $tipo = trim($_POST["tipo"]);
    $puertas = trim($_POST["puertas"]);
    $precio = trim($_POST["precio"]);
    $q = $pdo->prepare("UPDATE productos SET nombre=:nombre, descripcion=:descripcion, tipoCombustible=:tipo, cantidadPuertas=:puertas, precio=:precio WHERE id = :id");
    $q->bindParam(':id', $id);
    $q->bindParam(':nombre', $nombre);
    $q->bindParam(':descripcion', $descripcion);
    $q->bindParam(':tipo', $tipo);
    $q->bindParam(':puertas', $puertas);
    $q->bindParam(':precio', $precio);
    $q->execute();
}

if(isset($_GET["id"]) && !empty(trim($_GET["id"])))
{
    $_id = $_GET["id"];
    $select_id = $pdo->prepare("SELECT * FROM auto WHERE id = :id");
    $select_id->bindParam(':id', $id);
    $select_id->execute();
    $auto = $select_id->fetchAll(PDO::FETCH_OBJ);

    $_nombre = $auto[0]->nombre;
    $_descripcion = $auto[0]->descripcion;
    $_tipoCombustible = $auto[0]->tipoCombustible;
    $_cantidadPuertas = $auto[0]->cantidadPuertas;
    $_precio = $auto[0]->precio;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Producto</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">    
  <link rel="stylesheet" href="css/style.css">    
</head>
<body>        
<div class="container p-5">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
                <input type="hidden" name="id" value="<?=$_id?>">
                <label class="col-sm-2">Nombre</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" value="<?=$_nombre?>" >                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Descripcion</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="nombre" value="<?=$_descripcion?>" >                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Tipo Combustible</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="tipo" value="<?=$_tipoCombustible?>" >                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Cantidad Puertas</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="puertas" value="<?=$_cantidadPuertas?>" >                
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2">Precio</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="precio" value="<?=$_precio?>" >                
                </div>
            </div>            
        </div>
    </div>
</body>
</html>